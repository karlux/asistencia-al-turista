
/******************************************************************************
 * Funciones para request asincrónico y sincrónico utilizando XMLHttpRequest
 */
var asyncQuery = function(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        // https://stackoverflow.com/questions/13293617/why-is-my-ajax-function-calling-the-callback-multiple-times
        if (this.readyState === 4) {
            if (this.status === 200) {
                // parseamos el resultado para obtener el objeto JavaScript
                resObj = JSON.parse(xhttp.responseText)
                // llamamos a la función callback con el objeto parseado como parámetro.
                callback(resObj);
            }
        }
    };
    xhttp.open("GET", url, true);
    var ret = xhttp.send();
    return ret;
}

var syncQuery = function(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, false);
    // El browser (Chrome) dispara una excepción:
    // [Deprecation] Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental
    // effects to the end user's experience.
    // For more help, check https://xhr.spec.whatwg.org/.
    xhttp.send();

    if (xhttp.status === 200) {
        resObj = JSON.parse(xhttp.responseText)
        return resObj;
    }
    return null;
}

/******************************************************************************
 * Inicio.
 */
var bootstrap = function() {
    var url = Config.url;
	var urlTrucks = '/supporttrucks/';
	var urlStates = '/truckstates/';

    var map = createMap('mapid');
    var drawer = new Drawer();

    // OPCIÓN 1: Request asincrónico. *****************************************
    // dibujar los móviles de manera asincrónica
    
	//mostrar los estados	
	var callback = function(response) {
		drawer.drawStatesInList(response.states, 'states');
		
		var states = response.states.reduce(function(dict, state) {
			dict[state.id] = state;			
			return dict;
		}, {});

		// funcion para el movil, cuando ya tenemos los estados		
		var callback2 = function(response) {
			console.log(states);
			var supportTruck = response.supportTruck;

            console.log(supportTruck);

            supportTruck.state = states[response.supportTruck.state_id];            
            delete supportTruck.state_id;

            console.log(supportTruck);

			drawer.drawSupportTruckInMap(supportTruck, map);
        }
		
		//pedimos el movil 303, segundo pedido
		asyncQuery(url + urlTrucks + "303", callback2);
		//

    };
	
	//inicio del pedido
    asyncQuery(url + urlStates, callback);
	
    // Esta opción deriva en el callbackhell
    // Referencias:
    // 1. http://callbackhell.com/
    // 2. https://stackoverflow.com/questions/25098066/what-the-hell-is-callback-hell-and-how-and-why-rx-solves-it
    // FIN OPCIÓN 1 ***********************************************************

    // OPCIÓN 2: Request sincrónico.  *****************************************
    // dibujar los moviles de manera sincrónica
	var skip = function(response) {
	};
	
	//pedimos el movil 402
    var response1 = syncQuery(url + urlTrucks + "402", skip);
    
    if (response1) {
		var state_id = response1.supportTruck.state_id;
		
		var response2 = syncQuery(url + urlStates + state_id, skip);
		
        if (response2) {
		    var supportTruck = response1.supportTruck;
		    
            supportTruck.state = response2.state;
            delete supportTruck.state_id;

		    drawer.drawSupportTruckInMap(supportTruck, map);
        }    

    }

    // FIN OPCIÓN 2 ***********************************************************

};

$(bootstrap);
