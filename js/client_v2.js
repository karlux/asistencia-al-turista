
var bootstrap = function() {
    var url = Config.url;
	var urlTrucks = '/supporttrucks/';
	var urlStates = '/truckstates/';
	
    var map = createMap('mapid');
    var drawer = new Drawer();

    // OPCIÓN 3: Promises. Request asincrónico evitando el callbackhell.   ****
	
    var requestSupportTruck = function(supportTruck_id) {
        return $.ajax(url + urlTrucks + supportTruck_id);
    }
    var requestState = function(state_id) {
        return $.ajax(url + urlStates + state_id);
    }
    var responseExtract = function(attr, response) {
        console.log(response);
        return response[attr]
    }
    var extractSupportTruck = function(response) {
        return responseExtract('supportTruck', response);
    }
    var extractState = function(response) {
        return responseExtract('state', response);
    }
    var drawSupportTruck = function(supportTruck) {
		drawer.drawSupportTruckInMap(supportTruck, map);
    }

    var resolveStateSupportTruck = function(supportTruck) {
        // pedimos el estado con el state_id, y retornamos el movil completo
        return requestState(supportTruck.state_id)
               .then(function(response){
                    supportTruck.state = response.state;
                    delete supportTruck.state_id;                    
                    return supportTruck;        
                });
    }

    // comenzamos la ejecución:
	requestSupportTruck(606) 			// pedimos un movil al servidor
        .then(extractSupportTruck) 		// extraemos el movil de la respuesta del servidor 
        .then(resolveStateSupportTruck) // resolvemos el estado  
        .then(drawSupportTruck) 		// dibujamos el movil con su estado 
        .done(function() {
            console.log("Fin.");
        });

	/* 
	//forma equivalente
	requestSupportTruck(606) 			
        .then(resp =>extractSupportTruck(resp))        
		.then(truck =>resolveStateSupportTruck(truck))               
		.then(truck =>{console.log(truck);drawSupportTruck(truck)})
        .done(function() {
            console.log("Fin.");
        });
	*/


    // FIN OPCIÓN 3 ***********************************************************
};

$(bootstrap);
